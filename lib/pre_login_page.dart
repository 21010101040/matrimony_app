import 'package:flutter/material.dart';
import 'package:matrimony_app/loginpage.dart';
import 'package:matrimony_app/signuppage.dart';

class PreLoginPage extends StatelessWidget{
  const PreLoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(body: Column(
        children: [
          Expanded(
            child: Stack(
              fit: StackFit.expand,
              children: [
                Image.asset('assets/images/loginpage.jpg',fit: BoxFit.cover,),
                Container(
                  margin: EdgeInsets.only(top: 60),
                  child: Column(
                    children: [
                      Text(
                        "INDIA'S\nMOST TRUSTED\nMATRIMONY BRAND",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 32,fontFamily: 'Raleway-Regular'),
                      ),
                      Text("------------------------------------------------------------\nBY THE BRAND TRUST REPORT 2022\n------------------------------------------------------------",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 15,fontFamily: 'Raleway-Regular',fontWeight: FontWeight.bold),)
                    ],
                  ),
                )

              ],
            ),
          ),
          Row(
            children: [
            Expanded(child: Container(
                color: Color.fromARGB(255, 142, 196, 74),
                child: TextButton(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));},
                    child: Text("LOGIN",style: TextStyle(fontSize: 25,color: Colors.white),))
            )
            ),
              Expanded(child: Container(
                  color: Color.fromARGB(255, 48, 48, 48),
                  child: TextButton(onPressed: () {Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()));},
                    child: Text("SIGNUP",style: TextStyle(fontSize: 25,color: Colors.white)),
                  ),
                  )

              )

            ],
          ),
        ],
      ),),
    );
  }
  
}