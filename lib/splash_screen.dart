import 'dart:async';
import 'package:flutter/material.dart';
import 'package:matrimony_app/pre_login_page.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Timer(const Duration(seconds: 3), () {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (_) => const PreLoginPage()),
          (route) => false);
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(  child: Stack(
            fit: StackFit.expand,
            children: [
              Image.asset(
                "assets/images/Splash.jpg",
                fit: BoxFit.cover,
              ),
              Container(
                margin: EdgeInsets.only(top: 30),
                child: Column(
                  children: [
                    Text("WELCOME\nTO\nDARSHAN MATRIMONY APP",textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 25,fontFamily: 'Raleway-Regular',fontWeight: FontWeight.bold),),
                  ],
                ),
              )
            ],

          ),)


        ],

      ),
    );
  }
}
